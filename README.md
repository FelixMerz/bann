# bANN

Basic artificial neural network.
No backward propagation or other meta-parameter optimization, just learning rate decay.
Implemented in JavaScript.