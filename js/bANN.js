//architecture:
//array in the form of [number of inputs, neurons in layer 1, neurons in layer 2, ..., neurons in layer n, number of outputs]

//activation:
//either pass a custom activation function or pass sig, relu, lrelu or tanh as a parameter

//inputNorm:
//either pass a custom input normalization function (example: if all inputs should be between 0...1, but the dataset contains values between 0...255
//you may want to pass a function iterating through the vector/array and passing the value to sig)
//or pass false and the constructor will replace the inputNorm dummy with the place-holder function "pass".

//outputNorm:
//same as input norm, but for the net outputs

class Net{
	constructor(architecture, activation, inputNorm, outputNorm){
		this.layers = [];
		this.activation = activation;
		this.lr = 0.1;
		this.epoch = 0;
		this.inputNorm = inputNorm != false ? inputNorm : this.pass;
		this.outputNorm = outputNorm != false ? outputNorm : this.pass;
		for(var i=1; i<architecture.length; i++){
			this.layers.push([]);
			for(var k=0; k<architecture[i]; k++){
				var w = []
				for(var j=0; j<architecture[i-1]; j++){
					w.push(-1 + Math.random()*2);
				}
				this.layers[i-1].push(w);
			}
		}
	}
	
	//performs either an input normaliziation by a custom "inputNorm" function or calls place-holder function "pass"
	//iterates layer by layer to give the sum of the previous layer's output (vector) multiplied with the current layer's weights (vector)
	//returns either an output normalized by a custom "outputNorm" function or calls "pass"
	think(values){
		var layerOut = this.inputNorm(values);
		for(var i=0; i<this.layers.length; i++){
			var buffer = []
			for(var j=0; j<this.layers[i].length; j++){
				var workingval = 0
				workingval+=sum(elmul(layerOut, this.layers[i][j]));
				buffer.push(this.activation(workingval));
			}
			layerOut = buffer;
		}
		return this.outputNorm(layerOut);
	}
	
	//calculates the mean squared error for a provided dataset (input data and expected output for that input data)
	//every output value is subtracted from the expected output, the differences are squared, the squares are summed and the sum is divided by the number of summands
	//example: output o = [0.9, 0.5], expected output eo = [1, 0], difference: [-0.1, 0.5], squared: [0.01, 0.25], summed: 0.26, mean: 0.13
	getMSE(data, classifications){
		var mse = [];
		for(var i=0; i<data.length; i++){
			var e = elsub(classifications[i], this.think(data[i]));
			mse.push(sum(elmul(e, e)));
		}
		return sum(mse)/data.length;
	}
	
	//iterate through all weights, adjusting them one by one by the learning rate and taking the MSE before and after
	//if the MSE gets bigger after adjustment, twice the learning rate is subtracted from the weight (may overshoot)
	//example: MSE with weight w1 = 1 is 0.1, MSE with weights w1= 1.1 is 0.2 -> w1 is adjusted -(2*0.1) to 0.9, MSE is now ASSUMED to be better to save performance
	learn(data, classifications){
		this.epoch++;
		for(var i=0; i<this.layers.length; i++){
			for(var j=0; j<this.layers[i].length; j++){
				for(var k=0; k<this.layers[i][j].length; k++){
					var be = this.getMSE(data, classifications);
					this.layers[i][j][k] += this.lr;
					var ne = this.getMSE(data, classifications);
					if(be < ne)
						this.layers[i][j][k] -= 2*this.lr;
				}
			}
		}
		return(be);
	}
	
	//placeholder function for simple implementation of input and output normalizer functions
	pass(n){	return n;	}

	//loops for as long as "e" (the MSE [mean squared error] of the net) is larger than the specified target error "target"
	//down-corrects the learning rate (learning rate decay) if the MSE after learning is larger than the MSE before learning (caused by overshooting)
	targetLearn(data, classifications, target){
		var e = this.learn(data, classifications);
		while(e > target){
			var ne = this.learn(data, classifications);
			if(ne > e){
				this.lr /= 2;
				console.log("Adjusted learning rate to", this.lr + ".");
			}
			e = ne;
		}
		console.log("Target reached (" + e + " < " + target + ").");
		console.log("Net epoch:", this.epoch + ".");
	}
}
//element-wise vector multiplication, returns [a1*b1, a2*b2, ..., an*bn]
function elmul(a, b){
	var n = [];
	for(var i=0; i<a.length; i++){
		n.push(a[i]*b[i]);
	}
	return n;
}
//element-wise vector subtraction, returns [a1-b1, a2-b2, ..., an-bn]
function elsub(a, b){
	var n = [];
	for(var i=0; i<a.length; i++){
		n.push(a[i]-b[i]);
	}
	return n;
}
//sum across vector elements returns v1+v2+...+vn
function sum(a){
	var n = 0;
	for(var i=0; i<a.length; i++){
		n+=a[i];
	}
	return n;
}
//sigmoid function, returns between 0...1
function sig(n)		{	return 1/(1+Math.pow(Math.E, -n));	}
//leaky ReLU function, returns between -inf...inf
function lrelu(n)	{	return n > 0 ? n : n*0.1;			}
//ReLU function, returns between 0...inf
function relu(n)	{	return n > 0 ? n : 0;				}
//hyperbolic tangent, returns between -1...1
function tanh(n)	{	return (1-Math.pow(Math.E, -2*n))/(1+Math.pow(Math.E, -2*n));	}
//TODO: implement function softmax	

//example input data (XOR [exclusive OR] gate) for a net with two inputs; data consisting of four possible input combinations
//Note: An XOR gate outputs 0 when both inputs are equal and 1 when both inputs are different
var d = [[0, 0], [0, 1], [1, 0], [1, 1]];

//example output/classification/validation data for a net with one output; data consisting of four expected classifications
var c = [[0],[1],[1],[0]];

//example net initialization
var n = new Net([2, 10, 1], sig, false, false);

//to make the net learn, put "n.targetLearn(d, c, 0.05)" into the console
//to have the network classify something, put "n.think([0, 1])" or any other meaningful values into the console

//Note: sig is not the recommended activation function, but it's the "safest", meaning it will quite reliably converge, albeit slowly
//use relu for faster and more precise, but less reliable convergence